public class Room {
    private int roomNumber;
    private boolean available;
    private Guest guest;
    private Reservation reservation;

    public Room(int roomNumber) {
        this.roomNumber = roomNumber;
        available = true;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public boolean isAvailable() {
        return available;
    }

    public void bookRoom(Guest guest, Reservation reservation) {
        this.guest = guest;
        this.reservation = reservation;
        available = false;
    }

    public void checkOut() {
        guest = null;
        reservation = null;
        available = true;
    }

    public String toString() {
        return "Stanza " + roomNumber + ": " + (available ? "Disponibile" : "Occupata");
    }
}
