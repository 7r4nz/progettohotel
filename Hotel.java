import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hotel {
    private List<Room> rooms;
    private List<Reservation> reservations;

    public Hotel() {
        rooms = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            rooms.add(new Room(i));
        }
        reservations = new ArrayList<>();
    }

    public void displayAvailableRooms() {
        System.out.println("Stanze disponibili:");
        for (Room room : rooms) {
            if (room.isAvailable()) {
                System.out.println("Stanza " + room.getRoomNumber());
            }
        }
    }

    public void displayReservations() {
        System.out.println("Prenotazioni:");
        for (Reservation reservation : reservations) {
            System.out.println(reservation);
        }
    }

    public void makeReservation(Scanner scanner) {
        displayAvailableRooms();
        System.out.print("Inserisci il numero della stanza da prenotare: ");
        int roomNumber = scanner.nextInt();
        scanner.nextLine();

        if (roomNumber < 1 || roomNumber > 10) {
            System.out.println("Numero di stanza non valido.");
            return;
        }

        Room room = rooms.get(roomNumber - 1);
        if (room.isAvailable()) {
            System.out.print("Nome del cliente: ");
            String guestName = scanner.nextLine();
            System.out.print("Importo pagato: ");
            double amountPaid = scanner.nextDouble();
            scanner.nextLine();

            System.out.print("Permanenza (in giorni): ");
            int nights = scanner.nextInt();
            scanner.nextLine();

            Guest guest = new Guest(guestName);
            Reservation reservation = new Reservation(room, guest, amountPaid, nights);
            room.bookRoom(guest, reservation);

            reservations.add(reservation);
            System.out.println("Prenotazione effettuata con successo.");
        } else {
            System.out.println("La stanza selezionata è già prenotata.");
        }
    }
}
